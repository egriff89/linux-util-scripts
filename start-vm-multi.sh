#!/usr/bin/env bash

# ~/.local/util/start-vm-multi.sh
# Author: Eric Griffith
# https://gitlab.com/egriff89/linux-util-scripts

# Purpose: Start a VM configured with multiple QXL displays, each display receiving its own window.
#          This assumes you're using QEMU with virt-manager.

if [[ $# -eq 0 ]]; then
    echo "Please provide the name of a VM as it appears in virt-manager. Example: win10"
    exit 1
fi

VM=$1

# Check for running VMs
RUNNING=$(virsh -c qemu:///system list | grep -q "running")
if [[ $RUNNING -ne 0 ]]; then
    echo "No VMs are currently running! Start one and try again!"
    exit 1
else
    echo -e "VMs currently running:\n"
    virsh -c qemu:///system list
fi

# Start provided VM in fullscreen, each display getting its own window
echo -e "Launching VM with name '$VM'..." 
ID=$(virsh -c qemu:///system list | grep "$VM" | awk '{print $1}')
virt-viewer -f -c qemu:///system "$ID"
