#!/usr/bin/env bash

# ~/.local/util/rootless-docker.sh
# Author: Eric Griffith
# https://gitlab.com/egriff89/linux-util-scripts

# Setup Docker to run in rootless mode.

# Check if rootless mode is already configured
if [[ $(grep -q "$USER" /etc/subuid) -eq 0 ]]; then
    echo -e "\nDocker already configured for rootless usage! Exiting..."
    exit 0
fi

# Install packages if not already installed
if [[ $1 == "chaotic" ]]; then
    sudo pacman -S docker docker-compose chaotic-aur/docker-rootless-extras --needed --noconfirm
else
    sudo pacman -S docker docker-compose --needed --noconfirm
    paru -Sa docker-rootless-extras --needed --noconfirm
fi

# Configure rootless mode
echo -e "\nConfiguring Docker to run in rootless mode..."
echo "$USER:231072:65536" | sudo tee -a /etc/subuid
echo "$USER:231072:65536" | sudo tee -a /etc/subgid

# Add docker group if it doesn't exist
if [ "$(grep -q '^docker' /etc/group)" != 0 ]; then
   sudo groupadd docker
fi

# Add user to docker group and enable service
sudo usermod -aG docker "$USER"
systemctl --user enable --now docker.socket

/usr/bin/cat << EOF
Add the following export in your shell config if it doesn't exist already or
run it directly from the terminal:

   BASH/ZSH: export DOCKER_HOST=unix://\$XDG_RUNTIME_DIR/docker.sock
   FISH:     set DOCKER_HOST unix://\$XDG_RUNTIME_DIR/docker.sock

Source your config, if modified, or start a new terminal session for changes to take effect.
EOF
