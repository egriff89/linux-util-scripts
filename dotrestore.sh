#!/usr/bin/env bash

# ~/.local/util/dotrestore.sh
# Author: Eric Griffith
# Purpose: Restore dotfiles on fresh install
# https://gitlab.com/egriff89/linux-util-scripts

REPO='git@gitlab.com:egriff89/dotfiles.git'
DOTFILES="$HOME/.dotfiles"

# Clone and config repo
echo "Cloning dotfiles repo..."
/usr/bin/git clone -b master --bare "$REPO" "$DOTFILES"
/usr/bin/git --git-dir="$DOTFILES" --work-tree="$HOME" config --local status.showUntrackedFiles no

# Checkout master branch (Moves files into $HOME from $HOME/.dotfiles)
echo -e "\nMoving files to working tree..."
/usr/bin/git --git-dir="$DOTFILES" --work-tree="$HOME" checkout -f

echo -e "\nDONE! Please source the appropriate rc file for your shell"
