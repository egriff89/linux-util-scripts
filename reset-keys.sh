#!/usr/bin/env bash

# ~/.local/util/reset-keys.sh
# Author: Eric Griffith
# http:/https://gitlab.com/egriff89/linux-util-scripts

# Reset all GPG keys for pacman in case of corruption
# Taken from https://wiki.archlinux.org/title/Pacman/Package_signing

# Remove all GPG keys, reinit keys, and populate them
sudo rm -rf /etc/pacman.d/gnupg
sudo pacman-key --init
sudo pacman-key --populate

# Update the Arch Linux keyring
sudo pacman -S archlinux-keyring
