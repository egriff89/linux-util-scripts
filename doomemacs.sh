#!/usr/bin/env bash

# ~/.local/util/doomemacs.sh
# Author: Eric Griffith
# Purpose: Install DOOM Emacs
# http://gitlab.com/egriff89/linux-util-scripts

EMACSDIR=$HOME/.config/emacs
SERVICE_DIR=$HOME/.config/systemd/user

echo -e "Installing dependencies..."
sudo pacman -S git fd ripgrep emacs-nativecomp --needed --noconfirm

echo -e "\nInstalling DOOM Emacs..."
/usr/bin/git clone --depth 1 https://github.com/doomemacs/doomemacs "$EMACSDIR"
"$EMACSDIR"/bin/doom --force install
"$EMACSDIR"/bin/doom sync

echo -e "Creating daemon service..."
/usr/bin/mkdir -p "$SERVICE_DIR"

/usr/bin/cat << EOF > "$SERVICE_DIR"/emacs.service
[Unit]
Description=Emacs daemon
Documentation=info:emacs man:emacs(1) https://gnu.org/software/emacs/

[Service]
Type=forking
ExecStart=/usr/bin/emacs --daemon
ExecStop=/usr/bin/emacsclient --eval "(kill emacs)"
Environment=SSH_AUTH_SOCK=%t/keyring/ssh
Restart=on-failure

[Install]
WantedBy=default.target
EOF

# Enable and start service
systemctl enable --user --now emacs.service
