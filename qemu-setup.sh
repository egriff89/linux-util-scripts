#!/usr/bin/env bash

# ~/.local/util/qemu-setup.sh
# Author: Eric Griffith
# https://gitlab.com/egriff89/linux-util-scripts

# Adapted from Mental Outlaw's video "Ditch Virtualbox, Get QEMU/Virt Manager"
# Source: https://www.youtube.com/watch?v=wxxP39cNJOs

# Install and setup QEMU and Virt Manager

CONFIG='/etc/libvirt/libvirtd.conf'

# Install required packages
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 \
    bridge-utils libguestfs --needed --noconfirm

# Enable and start the service
sudo systemctl enable --now libvirtd

# Add user to libvirt group
sudo usermod -aG libvirt "$USER"

# Make necessary config changes
sudo sed -i 's/#unix_sock_ro/unix_sock_ro/' $CONFIG
sudo sed -i 's/#unix_sock_rw/unix_sock_rw/' $CONFIG

# Restart service
sudo systemctl restart libvirtd

echo -e "Please logout or reboot for the changes to take effect!\n"
