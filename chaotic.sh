#!/usr/bin/env bash

# ~/.local/util/chaotic.sh
# Author: Eric Griffith
# Purpose: Enable Chaotic AUR
# https://gitlab.com/egriff89/linux-util-scripts

# See https://aur.chaotic.cx/docs for more information

if grep -q 'chaotic-aur' /etc/pacman.conf; then
    echo -e "Chaotic-AUR already exits in /etc/pacman.conf!"
    grep -A2 'chaotic-aur' /etc/pacman.conf
    exit 0
fi

echo -e "Initialize pacman keyring...\n"
sudo pacman-key --init

KEY=3056513887B78AEB
KEYRING='https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'
MIRRORLIST='https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
echo -e "Installing Chaotic-AUR keyring and mirrorlist...\n"
sudo pacman-key --recv-key $KEY --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key $KEY
sudo pacman -U $KEYRING $MIRRORLIST --noconfirm

# Backup pacman.conf and add the Chaotic AUR
echo -e "Adding Chaotic-AUR to /etc/pacman.conf...\n"
sudo cp /etc/pacman.conf /etc/pacman.conf.bak
echo -e "\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist" | sudo tee -a /etc/pacman.conf

# Sync the repos
echo -e "Syncing the repositories...\n"
sudo pacman -Syy
