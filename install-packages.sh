#!/usr/bin/env bash

# ~/.local/util/install-packages.sh
# Author: Eric Griffith
# Purpose: Install additional packages upon new installation of any Arch-based distro
# http://gitlab.com/egriff89/linux-util-scripts

UTILDIR=$HOME/.local/util
DATAFILES=$UTILDIR/datafiles
AURDIR=$HOME/aur

if [ ! -d "$AURDIR" ]; then
   mkdir "$AURDIR"
fi

# Install other packages
echo "Installing standard packages..."
PKGS=$(grep -Ev '^#|^chaotic' "$DATAFILES/pkgs.txt")
for pkg in $PKGS; do
   sudo pacman -S "$pkg" --needed --noconfirm
done

# Check for existence of Chaotic-AUR and install packages
echo -e "\nChecking if Chaotic-AUR is enabled..."
CA="$(grep -q '^\[chaotic-aur' /etc/pacman.conf)"
CAPKGS=$(grep '^chaotic' "$DATAFILES/pkgs.txt")

if [[ $CA -eq 0 ]]; then
  echo -e "\nInstalling Chaotic-AUR packages..."
  for pkg in $CAPKGS; do
     sudo pacman -S "$pkg" --needed --noconfirm
  done
else
   echo -e "\nChaotic-AUR not enabled! Continuing..."
fi

# Install AUR helper
echo -e "\nInstalling Paru..."
/usr/bin/git clone http://aur.archlinux.org/paru-bin "$AURDIR/paru-bin"
cd "$AURDIR/paru-bin" || exit
/usr/bin/makepkg -si
cd "$UTILDIR" || exit

# Install AUR packages
echo -e "\nInstalling AUR packages..."
AURPKGS=$(awk '{print $1}' "$UTILDIR/datafiles/aur.txt" | grep -v '^#')
for pkg in $AURPKGS; do
   paru -Sa "$pkg" --needed --noconfirm
done

